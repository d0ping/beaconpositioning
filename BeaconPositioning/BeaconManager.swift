//
//  BeaconPositioningManager.swift
//  BeaconPositioning
//
//  Created by Denis Bogatyrev on 04/10/2018.
//
//  The MIT License (MIT)
//  Copyright (c) 2018 Denis Bogatyrev.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
//  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
//  and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//

import UIKit
import CoreLocation

enum CalculateAcuracyMode {
    case base // Apple approach
    case custom
}

struct BeaconMeta {
    var major: Int = 0
    var minor: Int = 0
    
    init(major: Int = 0, minor: Int = 0) {
        self.major = major
        self.minor = minor
    }
}

protocol BeaconManager {
    var isMonitoring: Bool {get}
    
    var mode: CalculateAcuracyMode {get set}
    var maxDistance: Double {get set} // in meters
    
    var delegate: BeaconManagerDelegate? {get set}
    
    func configure(regionUUID: UUID, beacons: [BeaconMeta], updatedTime: TimeInterval)
    func startMonitoring()
    func stopMonitoring()
}

protocol BeaconManagerDelegate {
    func didEnterBeaconRegion(_ regionUUID: UUID)
    func didExitBeaconRegion(_ regionUUID: UUID)
    func beaconAccuracyPeriodicalUpdate(_ accuracies: [CLLocationAccuracy], accuraciesAvg: [CLLocationAccuracy])
    func proximityDidUpdate(_ proximities: [CLProximity])
    func didChangeNearestBeaconAtIndex(_ index: Int)
}


class BeaconManagerImplementation: NSObject, BeaconManager, CLLocationManagerDelegate {
    var isMonitoring: Bool = false
    var maxDistance: Double = 10
    private var updateTime: TimeInterval = 1
    var delegate: BeaconManagerDelegate?
    
    var mode:CalculateAcuracyMode = .base
    
    var monitoringRegionUUID: UUID?
    var monitoringBeaconMeta: [BeaconMeta] = []
    
    private let locationManager = CLLocationManager()
    private var timer: Timer?
    
    var monitoringRegion: CLBeaconRegion?
    let kMonitoringRegionIdentifiir = UUID().uuidString
    
    var accuracies: [CLLocationAccuracy] = []
    var accuraciesAvg: [[CLLocationAccuracy]] = []
    let kAccuracyAvgCount = 5
    var nearestBeaconIndex: Int?
    
    var lastUpdate: [Date] = []
    
    var proximities: [CLProximity] = []
    
    func configure(regionUUID: UUID, beacons: [BeaconMeta], updatedTime: TimeInterval) {
        self.updateTime = updatedTime
        self.monitoringRegionUUID = regionUUID
        self.monitoringBeaconMeta = beacons
        
        
        self.monitoringRegion = CLBeaconRegion(proximityUUID: regionUUID, identifier: kMonitoringRegionIdentifiir)
        
        // Fill empty data
        for _ in beacons {
            self.accuracies.append(0)
            self.accuraciesAvg.append([])
            self.proximities.append(.unknown)
            self.lastUpdate.append(Date())
        }
    }
    
    func startMonitoring() {
        guard let region = self.monitoringRegion else {
            // Error
            return
        }
        
        self.locationManager.startMonitoring(for: region)
        self.locationManager.startRangingBeacons(in: region)
        
        self.locationManager.delegate = self
        self.locationManager.pausesLocationUpdatesAutomatically = false
//        self.locationManager.allowsBackgroundLocationUpdates = true
        
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedAlways) {
            locationManager.requestAlwaysAuthorization()
        }
        
        self.locationManager.startUpdatingLocation()
        
        self.isMonitoring = true
        
        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(withTimeInterval: self.updateTime, repeats: true, block: { [weak self] timer in
            
            // Filter old accuracies
            var index = 0
            for updateStamp in (self?.lastUpdate)! {
                if updateStamp.timeIntervalSinceNow * -1 >= 15 {
                    self?.accuracies[index] = (self?.maxDistance)!
                    self?.accuraciesAvg[index].append((self?.maxDistance)!)
                    if (self?.accuraciesAvg[index].count)! > (self?.kAccuracyAvgCount)! {
                        self?.accuraciesAvg[index].removeFirst()
                    }
                    
                    self?.lastUpdate[index] = Date()
                }
                index += 1
            }
            
            // Calculate neares Beacon
            var avgAccurMin: Double = 100
            var minIndex: Int?
            index = 0
            
            // Get average accuracy
            var avg: [CLLocationAccuracy] = []
            for arr in (self?.accuraciesAvg)! {
                if arr.count == 0 {
                    avg.append(0)
                    continue
                }
                var sum: CLLocationAccuracy = 0
                for accur in arr {
                    sum += accur
                }
                let avgAccur = sum / Double(arr.count)
                avg.append(avgAccur)
                
                if avgAccurMin > avgAccur {
                    avgAccurMin = avgAccur
                    minIndex = index
                }
                index += 1
            }
            
            if self?.nearestBeaconIndex != minIndex {
                self?.nearestBeaconIndex = minIndex
                self?.delegate?.didChangeNearestBeaconAtIndex((self?.nearestBeaconIndex)!)
            }
            
            // send regiones accurancy
            self!.delegate?.beaconAccuracyPeriodicalUpdate(self!.accuracies, accuraciesAvg: avg)
        })
    }
    
    func stopMonitoring() {
        self.timer?.invalidate()
        self.timer = nil
        
        self.locationManager.stopUpdatingLocation()
        self.locationManager.delegate = nil
        
        self.isMonitoring = false
        
        if let region = self.monitoringRegion {
            self.locationManager.stopMonitoring(for: region)
            self.locationManager.stopRangingBeacons(in: region)
        }
    }
    
    
    // MARK: - CLLocationManagerDelegate
    public func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if region.identifier == kMonitoringRegionIdentifiir {
            if let regionUUID = self.monitoringRegionUUID {
                self.delegate?.didEnterBeaconRegion(regionUUID)
            }
        }
    }

    public func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        if region.identifier == kMonitoringRegionIdentifiir {
            if let regionUUID = self.monitoringRegionUUID {
                self.delegate?.didExitBeaconRegion(regionUUID)
            }
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        var proximityChanged = false
        guard let regionUUID = self.monitoringRegionUUID else {
            return
        }
        if regionUUID != region.proximityUUID {
            return
        }
        
        var index: Int = 0
        for beaconMeta in self.monitoringBeaconMeta {
            for beacon in beacons {
                if beacon.major.intValue == beaconMeta.major && beacon.minor.intValue == beaconMeta.minor {
                    let accuracy = beacon.accuracy
                    if accuracy > 0 {
//                        print(String(format: "%.2f - %.2f", accuracy, self.calculateDistance(rssi: beacon.rssi)))
                        var value: CLLocationAccuracy = 0
                        switch self.mode {
                        case .custom:
                            value = self.calculateDistance(rssi: beacon.rssi)
                        case .base:
                            value = accuracy
                        }
                        
                        self.accuracies[index] = value
                        
                        let filtredValue = (value < 10 ? value : 10)
                        self.accuraciesAvg[index].append(filtredValue)
                        if self.accuraciesAvg[index].count > kAccuracyAvgCount {
                            self.accuraciesAvg[index].removeFirst()
                        }
                        
                        self.lastUpdate[index] = Date()
                        
                        let proximity = beacon.proximity
                        if proximity != self.proximities[index] {
                            proximityChanged = true
                            self.proximities[index] = proximity
                        }
//                        print(String(format: "%zd - %.2f (%@)", index, self.accuracies[index], self.proximityDescription(proximity) ))
                    }
                    break
                }
            }
            index += 1
        }
        if proximityChanged {
            self.delegate?.proximityDidUpdate(self.proximities)
        }
    }
    
    // MARK: Helpers
    private func calculateDistance(rssi: Int) -> CLLocationAccuracy {
        if (rssi == 0) { return -1.0 }
        
        let txPower = -59 //hard coded power value. Usually ranges between -59 to -65
        let ratio = Float(Float(rssi) / Float(txPower))
        if (ratio < 1.0) { return Double(pow(ratio, 10)) }
        
        return Double( (0.89976) * pow(ratio, 7.7095) + 0.111)
    }
    
    func proximityDescription(_ proximity: CLProximity) -> String {
        var proximityString = String()
        switch proximity {
        case .immediate:
            proximityString = "Близко"
        case .near:
            proximityString = "Недалеко"
        case .far:
            proximityString = "Далеко"
        case .unknown:
            proximityString = "Unknown"
        }
        return proximityString
    }
    
}
