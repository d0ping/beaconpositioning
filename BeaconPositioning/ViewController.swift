//
//  ViewController.swift
//  BeaconPositioning
//
//  Created by Denis Bogatyrev on 04/10/2018.
//
//  The MIT License (MIT)
//  Copyright (c) 2018 Denis Bogatyrev.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
//  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
//  and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, BeaconManagerDelegate {
    
    @IBOutlet weak var coverView: UIImageView!
    @IBOutlet weak var drawingView: UIImageView!
    @IBOutlet weak var drawingBeaconView: UIImageView!
    
    @IBOutlet weak var point: UIView!
    @IBOutlet weak var pointX: NSLayoutConstraint!
    @IBOutlet weak var pointY: NSLayoutConstraint!
    @IBOutlet weak var pointXLabel: UILabel!
    @IBOutlet weak var pointYLabel: UILabel!
    
    @IBOutlet weak var accuracyALabel: UILabel!
    @IBOutlet weak var accuracyBLabel: UILabel!
    @IBOutlet weak var accuracyCLabel: UILabel!
    
    @IBOutlet weak var monitoringButton: UIButton!
    
    let beaconManager = BeaconManagerImplementation()
    let distanceFormatter = LengthFormatter()
    
    var nearestBeaconIndex: Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.point.layer.cornerRadius = 3
        self.point.layer.borderColor = UIColor.black.cgColor
        self.point.layer.borderWidth = 1
        self.point.clipsToBounds = true
        
        var beacons: [BeaconMeta] = []
        beacons.append(BeaconMeta(major:0, minor:0))
        beacons.append(BeaconMeta(major:0, minor:1))
        beacons.append(BeaconMeta(major:0, minor:2))
        
        self.beaconManager.delegate = self
        self.beaconManager.mode = .custom
        self.beaconManager.configure(regionUUID: UUID(uuidString: "E2C56DB5-DFFB-48D2-B060-D0F5A71096E0")!, beacons: beacons, updatedTime: 1)
        
        self.pointX.constant = self.coverView.frame.size.width * 0.5
        self.pointY.constant = self.coverView.frame.size.height * -0.5
        self.view.layoutIfNeeded()
    }

    @IBAction func toggleMonitopingButton(_ sender: UIButton) {
        let isMonitoring = self.beaconManager.isMonitoring
        
        if (!isMonitoring) {
            self.drawingView.image = nil
            self.beaconManager.startMonitoring()
        } else {
            self.drawingBeaconView.image = nil
            self.beaconManager.stopMonitoring()
        }
        
        sender.isSelected = !isMonitoring
    }
    
    // MARK: - BeaconPositioningManagerDelegate
    
    func didEnterBeaconRegion(_ regionUUID: UUID) {
        let alertController = UIAlertController(title: "Monotoring", message: "Enter region " + regionUUID.uuidString, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func didExitBeaconRegion(_ regionUUID: UUID) {
        let alertController = UIAlertController(title: "Monotoring", message: "Exit region " + regionUUID.uuidString, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func beaconAccuracyPeriodicalUpdate(_ accuracies: [CLLocationAccuracy], accuraciesAvg: [CLLocationAccuracy]) {
        self.accuracyALabel.text = String(format: "%@ (%@)", self.distanceFormatter.string(fromMeters: accuracies[0]), self.distanceFormatter.string(fromMeters: accuraciesAvg[0]))
        self.accuracyBLabel.text = String(format: "%@ (%@)", self.distanceFormatter.string(fromMeters: accuracies[1]), self.distanceFormatter.string(fromMeters: accuraciesAvg[1]))
        self.accuracyCLabel.text = String(format: "%@ (%@)", self.distanceFormatter.string(fromMeters: accuracies[2]), self.distanceFormatter.string(fromMeters: accuraciesAvg[2]))
        
        let size = CGSize(width: 12, height: 8)
        
        let pointInMeters: [CGPoint] = [CGPoint(x: size.width * 0.83, y: size.height * 0.98),
                             CGPoint(x: size.width * 0.52, y: size.height * 0.03),
                             CGPoint(x: size.width * 0.1, y: size.height * 0.98)]
        
        let position = self.coordinate(a: pointInMeters[0], b: pointInMeters[1], c: pointInMeters[2],
                                       dA: CGFloat(accuraciesAvg[0]), dB: CGFloat(accuraciesAvg[1]), dC: CGFloat(accuraciesAvg[2]))
        
        let prewPoint = self.point.center
        
        self.pointXLabel.text = String(format: "%.2f", position.x)
        self.pointYLabel.text = String(format: "%.2f", position.y)
        
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 1) {
            self.pointX.constant = self.coverView.frame.size.width * ( position.x / size.width )
            self.pointY.constant = self.coverView.frame.size.height * -1 * ( position.y / size.height )
            self.view.layoutIfNeeded()
            
            self.drawLine(fromPoint: prewPoint, toPoint: self.point.center)
            
            let coverSize = self.drawingBeaconView.frame.size
            let offset = CGFloat( (self.drawingBeaconView.frame.size.height - self.coverView.frame.size.height )/2 ) + self.coverView.frame.size.height
            let pointInPx = [CGPoint(x: coverSize.width * (pointInMeters[0].x / size.width ), y:offset + coverSize.height * -1 * (pointInMeters[0].y / size.width )),
                             CGPoint(x: coverSize.width * (pointInMeters[1].x / size.width ), y:offset + coverSize.height * -1 * (pointInMeters[1].y / size.width )),
                             CGPoint(x: coverSize.width * (pointInMeters[2].x / size.width ), y:offset + coverSize.height * -1 * (pointInMeters[2].y / size.width ))]
            let distances = [CGFloat(coverSize.width * (CGFloat(accuraciesAvg[0]) / size.width )),
                             CGFloat(coverSize.width * (CGFloat(accuraciesAvg[1]) / size.width )),
                             CGFloat(coverSize.width * (CGFloat(accuraciesAvg[2]) / size.width ))]
            
            self.drawBeaconCicles(beacons: pointInPx, distances: distances)
        }
        
    }
    
    func proximityDidUpdate(_ proximities: [CLProximity]) {
        var index = 0
        for proximity in proximities {
            var color: UIColor = .clear
            switch proximity {
            case .far:
                color = .red
            case .near:
                color = .yellow
            case .immediate:
                color = .green
            case .unknown:
                color = .clear
            }
            
            if index == 0 {
                self.accuracyALabel.backgroundColor = color
            } else if index == 1 {
                self.accuracyBLabel.backgroundColor = color
            } else if index == 2 {
                self.accuracyCLabel.backgroundColor = color
            }
            index += 1
        }
        
    }
    
    func didChangeNearestBeaconAtIndex(_ index: Int) {
        self.nearestBeaconIndex = index
        
    }

    // MARK: Helpers
    private func coordinate(a: CGPoint, b: CGPoint, c: CGPoint, dA: CGFloat, dB: CGFloat, dC: CGFloat ) -> CGPoint {
        let W = dA*dA - dB*dB - a.x*a.x - a.y*a.y + b.x*b.x + b.y*b.y
        let Z = dB*dB - dC*dC - b.x*b.x - b.y*b.y + c.x*c.x + c.y*c.y

        let x = (W*(c.y-b.y) - Z*(b.y-a.y)) / (2 * ((b.x-a.x)*(c.y-b.y) - (c.x-b.x)*(b.y-a.y)))
        var y = (W - 2*x*(b.x-a.x)) / (2*(b.y-a.y))
        //y2 is a second measure of y to mitigate errors
        let y2 = (Z - 2*x*(c.x-b.x)) / (2*(c.y-b.y))

        y = (y + y2) / 2

        return CGPoint(x: x, y: y)
    }

    private func drawLine(fromPoint: CGPoint, toPoint: CGPoint) {
        
        UIGraphicsBeginImageContext(self.drawingView.bounds.size)
        
        guard let context = UIGraphicsGetCurrentContext() else { return }
        
        let color = UIColor.red
        let lineWidth: CGFloat = 2
        
        let currentImageView: UIImageView = self.drawingView
        
        self.drawingView.image?.draw(in: CGRect(origin: .zero, size: self.drawingView.bounds.size))
        
        context.move(to: fromPoint)
        context.addLine(to: toPoint)
        context.setLineCap(.round)
        context.setLineWidth(lineWidth)
        context.setStrokeColor(color.cgColor)
        context.setBlendMode(.normal)
        context.strokePath()
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        currentImageView.image = image
        
        UIGraphicsEndImageContext()
    }
    
    private func drawBeaconCicles(beacons: [CGPoint], distances: [CGFloat]) {
        let imageView: UIImageView = self.drawingBeaconView
        
        UIGraphicsBeginImageContext(imageView.bounds.size)
        
        guard let context = UIGraphicsGetCurrentContext() else { return }
        
        var index = 0
        for point in beacons {
            let isNearest: Bool = (self.nearestBeaconIndex == index)
            
            context.setFillColor(isNearest ?  UIColor.green.withAlphaComponent(0.4).cgColor : UIColor.orange.withAlphaComponent(0.2).cgColor)
            context.setStrokeColor(UIColor.purple.withAlphaComponent(0.75).cgColor)
            context.setLineWidth(1)
            
            let dist = distances[index]
            context.addEllipse(in: CGRect( x: point.x - dist, y: point.y - dist, width: dist*2, height: dist*2))
            context.drawPath(using: .fillStroke)
            
            context.setFillColor(isNearest ? UIColor.green.cgColor : UIColor.red.cgColor)
            context.setStrokeColor(isNearest ? UIColor.green.cgColor : UIColor.red.cgColor)
            context.setLineWidth(isNearest ? 5 : 2)
            
            context.addRect(CGRect( x: point.x - 1, y: point.y - 1, width: 2, height: 2))
            context.drawPath(using: .fillStroke)
            
            index += 1
        }
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        imageView.image = image
        
        UIGraphicsEndImageContext()
    }
}

